FROM golang:1.13.7

WORKDIR /go/src/app
COPY . .

RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
RUN dep ensure

ENTRYPOINT go run main.go