package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"github.com/gocolly/colly"
)

type powerEvent struct {
	Title    string `json:"title"`
	Date     string `json:"date"`
	Location string `json:"location"`
	URL      string `json:"url"`
}

var collector = colly.NewCollector(colly.Async(true))

func findEventURLs(eventPageURL string) []string {
	clone := collector.Clone()

	var results []string
	clone.OnXML("//div[@class='Content']//div[contains(@class, 'no-pad')]", func(e *colly.XMLElement) {
		url := e.ChildAttr("/h3/a", "href")
		if url != "" {
			results = append(results, url)
		}
	})

	clone.Visit(eventPageURL)
	clone.Wait()

	return results
}

func visitEventPageURL(eventURL string) powerEvent {
	clone := collector.Clone()

	result := powerEvent{URL: eventURL}
	clone.OnXML("//div[@id='MPContentArea']", func(e *colly.XMLElement) {
		// extract title
		result.Title = e.ChildText("/h1")

		candidates := e.ChildTexts("//span[contains(text(), 'When:')]/parent::div/text()")
		// extract date from first non-empty results
		for _, cand := range candidates {
			if cand != "" {
				result.Date = strings.ReplaceAll(cand, " from", ";")
				break
			}
		}

		// extract location
		locationParts := e.ChildTexts("//h2[text()='Location']/parent::div/div[1]/div[1]/text()")
		result.Location = strings.Join(locationParts[:len(locationParts)-1], " ")
	})

	clone.Visit(eventURL)
	clone.Wait()

	fmt.Println(result)
	return result
}

func main() {

	eventPageURLs := []string{
		"https://www.flowug.com/flowusergroup/communities/community-home/recent-community-events?communitykey=e58b842b-b6bb-403f-a7f5-91305a367441&tab=recentcommunityeventsdashboard",
		"https://www.pbiusergroup.com/powerbiug/communities/community-home/recent-community-events?communitykey=a739c5bc-f948-4e49-9eb8-b8b0bf7eeaa1&tab=recentcommunityeventsdashboard",
		"https://www.powerappsug.com/powerappsusergroup/communities/community-home/recent-community-events?communitykey=6ad17dfc-1414-4a3d-8a7c-b0f006ffc466&tab=recentcommunityeventsdashboard",
		"https://www.crmug.com/crmug/communities/community-home/recent-community-events?communitykey=5c2a5e68-d02d-4746-b11b-d04bbe0da56a&tab=recentcommunityeventsdashboard",
		"https://www.crmug.com/crmug/communities/community-home/recent-community-events?communitykey=9ce1f0f8-8eab-4bab-9636-f1e7742846d9&tab=recentcommunityeventsdashboard",
		"https://www.crmug.com/crmug/communities/community-home/recent-community-events?communitykey=7af1a416-c1fa-4eb1-9ef3-b52f40e53d3d&tab=recentcommunityeventsdashboard",
		"https://www.pbiusergroup.com/powerbiug/communities/community-home/recent-community-events?communitykey=8727f1ad-1ac9-41dd-ad32-7db23a740378&tab=recentcommunityeventsdashboard",
	}

	// first crawl event listing pages
	var eventURLs []string
	for _, site := range eventPageURLs {
		eventURLs = append(eventURLs, findEventURLs(site)...)
	}

	// then crawl each individual event URL
	var events []powerEvent
	for _, eventURL := range eventURLs {
		events = append(events, visitEventPageURL(eventURL))
	}

	i := 0
	format := "Jan 2, 2006"
	// filter out events that are in the past
	for _, event := range events {
		testDate, err := time.Parse(format, strings.Split(event.Date, ";")[0])
		if err != nil {
			log.Fatal(err)
		}

		if !testDate.Before(time.Now()) {
			events[i] = event
			i++
		}
	}
	events = events[:i]

	// marshal to JSON
	all, err := json.Marshal(events)
	if err != nil {
		log.Fatal(err)
	}

	// log results
	fmt.Println(string(all))

	// write to disk
	ioutil.WriteFile(fmt.Sprintf("%s.json", time.Now().Format("Jan-2-2006")), all, 0644)
}
